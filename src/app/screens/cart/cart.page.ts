import { Component, OnInit } from '@angular/core';
import { AlertController, ToastController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { CartItem } from 'src/app/models/cart-item.module';
import { CartService } from 'src/app/services/cart.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.page.html',
  styleUrls: ['./cart.page.scss'],
})
export class CartPage implements OnInit {
  cartItemsPhp: Observable<CartItem[]>;
  totalAmountPhp: Observable<number>;
  status: string="";

  constructor(
    private cartService: CartService, 
    private alertCtrl: AlertController,
    private toastCtrl: ToastController,
    private alert: AlertController
    ) { }

  ngOnInit() {
    this.cartItemsPhp = this.cartService.getCart();
    this.totalAmountPhp = this.cartService.getTotalAmount();
  };


  onIncrease(item: CartItem) {
    this.cartService.changeQty(1, item.id);
  }

  onDecrease(item: CartItem) {
    if (item.quantity === 1) this.removeFromCart(item);
    else this.cartService.changeQty(-1, item.id);
  }
  
  async removeFromCart(item: CartItem) {
    const alert = await this.alertCtrl.create({
      header: 'Remove',
      message: 'Are you sure you want to remove?',
      buttons: [
        {
          text: 'Yes',
          handler: () => this.cartService.removeItem(item.id),
        },
        {
          text: 'No',
        },
      ],
    });
    
    alert.present();
  }

  openCartDialogue() {
    this.alert.create({
      header:"Confirm!",
      subHeader:"Are you sure?",
      buttons:[
        {
          text:"Cancel",
          handler:(data)=>{
            this.status= "Confirm Cancel"
          }
        },
        {
          text:"OK",
          handler: (data)=>{
            this.status="Order Successful"
          }
        }
      ]
    }).then((confirmElement)=>{
      confirmElement.present();
    })
  }

  
}

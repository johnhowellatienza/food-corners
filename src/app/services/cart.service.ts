import { Injectable } from "@angular/core";
import { BehaviorSubject} from "rxjs";
import { CartItem } from "../models/cart-item.module";
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class CartService {
    private itemsPhp = new BehaviorSubject<CartItem[]>([]);

    getCart() {
        return this.itemsPhp.asObservable();
    }

    addToCart(newItem: CartItem) {
        this.itemsPhp.next([ ...this.itemsPhp.getValue(), newItem]);
    }

    removeItem(id: number) {
        this.itemsPhp.next(this.itemsPhp.getValue().filter((item) => item.id !== id));
    }

    changeQty(quantity: number, id : number) {
        const items = this.itemsPhp.getValue();
        const index = items.findIndex((item) => item.id === id);
        items[index].quantity += quantity;
        this.itemsPhp.next(items);
    }

    getTotalAmount() {
        return this.itemsPhp.pipe(map(items => {
            let total = 0;
            items.forEach(item => {
                total += item.quantity * item.price;
            });

            return total;
        })
        );
    }
}
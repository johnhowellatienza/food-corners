import { Injectable } from "@angular/core";
import { Food } from "../models/food.model";

@Injectable({
    providedIn: 'root',
})
export class FoodService {
    getFoods(): Food[] {
        return [
            {
                id: 1,
                title: 'Chicken and Rice',
                price: 169,
                image: 'https://media.istockphoto.com/photos/grilled-steak-with-baked-potatoes-and-vegetables-picture-id889913376?k=6&m=889913376&s=612x612&w=0&h=vtmOtqoIFoBcPVE3rvbCvPlDyZLySOoZFglYqkgwz9E=',
                description: 'The Hainanese chicken rice is a dish that consists of succulent steamed white chicken cut into bite-size pieces and served on fragrant rice with some light soy sauce.',
            },
            {
                id: 2,
                title: 'Pork and Vegtable',
                price: 250,
                image: 'https://d3ur40406gizl1.cloudfront.net/web_site/images/v2/plates/4.png',
                description: 'The Hainanese chicken rice is a dish that consists of succulent steamed white chicken cut into bite-size pieces and served on fragrant rice with some light soy sauce.',
            },
            {
                id: 3,
                title: 'Chicken with Potato',
                price: 150,
                image: 'https://www.diabetesfoodhub.org/system/user_files/Images/1837-diabetic-pecan-crusted-chicken-breast_JulAug20DF_clean-simple_061720.jpg',
                description: 'The Hainanese chicken rice is a dish that consists of succulent steamed white chicken cut into bite-size pieces and served on fragrant rice with some light soy sauce.',
            },
            {
                id: 4,
                title: 'Grilled Pork',
                price: 200,
                image: 'http://asterseniorcommunities.com/stonecroft/wp-content/uploads/2017/03/plate-food.jpg',
                description: 'The Hainanese chicken rice is a dish that consists of succulent steamed white chicken cut into bite-size pieces and served on fragrant rice with some light soy sauce.',
            },
            {
                id: 5,
                title: 'Beef Steak',
                price: 209,
                image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQslAdlU3JVK4E1_KVBJozapOyis-WzjG6-mowVorNZiVYNplB8ThR6L6nlsx8gEBbZPCM&usqp=CAU',
                description: 'The Hainanese chicken rice is a dish that consists of succulent steamed white chicken cut into bite-size pieces and served on fragrant rice with some light soy sauce.',
            },
            {
                id: 6,
                title: 'Italian Pasta',
                price: 190,
                image: 'https://media.istockphoto.com/photos/pasta-with-meat-tomato-sauce-and-vegetables-picture-id898095706?k=6&m=898095706&s=612x612&w=0&h=yiVphslyffJUhLG_6HKXQnECKwKIjRArLOI5E9HMn5s=',
                description: 'Italian pasta is a collective name for food made from wheat flour and water. The name refers to the resulting dough (pasta also literally means "dough") of which different shapes are rolled and cut',
            },
            {
                id: 7,
                title: 'Spaghetti with Leaves',
                price: 150,
                image: 'https://i.pinimg.com/originals/78/11/25/7811254ad5ec751a44d3343f40be2c0b.png',
                description: 'Italian pasta is a collective name for food made from wheat flour and water. The name refers to the resulting dough (pasta also literally means "dough") of which different shapes are rolled and cut',
            },
            {
                id: 8,
                title: 'Filipino Pasta',
                price: 189,
                image: 'https://assets.bonappetit.com/photos/5cddd90b634f1e65e14578f9/16:9/w_2560%2Cc_limit/Basically-Shrimp-Scampi-Recipe.jpg',
                description: 'Italian pasta is a collective name for food made from wheat flour and water. The name refers to the resulting dough (pasta also literally means "dough") of which different shapes are rolled and cut',
            },
            {
                id: 9,
                title: 'Cookie D-Letche',
                price: 150,
                image: 'https://media.istockphoto.com/photos/crepes-with-strawberries-and-cream-picture-id871501924?k=6&m=871501924&s=612x612&w=0&h=JgkMVlbwAK9kj3xCW5HhEgOIAvti8YJ22GomHkKpdEw=',
                description: 'dessert, the last course of a meal. In the United States dessert is likely to consist of pastry, cake, ice cream, pudding, or fresh or cooked fruit',
            },
            {
                id: 10,
                title: 'Salad Cake',
                price: 195,
                image: 'https://i.pinimg.com/originals/87/ef/c2/87efc25610dd498c382d0246407bc2f6.jpg',
                description: 'dessert, the last course of a meal. In the United States dessert is likely to consist of pastry, cake, ice cream, pudding, or fresh or cooked fruit',
            },
            {
                id: 11,
                title: 'Sweet Donut',
                price: 120,
                image: 'https://i.pinimg.com/564x/04/a3/17/04a3173a441286723691737c80992dd8.jpg',
                description: 'dessert, the last course of a meal. In the United States dessert is likely to consist of pastry, cake, ice cream, pudding, or fresh or cooked fruit',
            },
            {
                id: 12,
                title: 'Matcha Tea',
                price: 110,
                image: 'https://previews.123rf.com/images/baibaz/baibaz1909/baibaz190900004/129615327-cup-of-green-tea-matcha-latte-with-heart-shaped-art-isolated-on-white-background-top-view.jpg',
                description: 'Most teas matches its description like Creamy Caramel or Sweet Lemon, which makes it easy to identify the taste before even tasting it.',
            },
            {
                id: 13,
                title: 'Calamansi Juice',
                price: 109,
                image: 'https://live.staticflickr.com/810/26007475837_8b8971ca64_c.jpg',
                description: 'Most teas matches its description like Creamy Caramel or Sweet Lemon, which makes it easy to identify the taste before even tasting it.',
            },
            {
                id: 14,
                title: 'Iced Tea',
                price: 99,
                image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcS8Uiy0AphXiMBN7yFjBNzO7P8N6utVuGJdEUhFO2BBZZNmG1oIB8dU_ZFLpQBnebC7Fn0&usqp=CAU',
                description: 'Most teas matches its description like Creamy Caramel or Sweet Lemon, which makes it easy to identify the taste before even tasting it.',
            },
        ];
    }

    getFood(id:number): Food{
        return this.getFoods().find((food) => food.id === id);
    }
}